import os
import logging
from models import FileNode, Label

class DirectoryIndexer:
    def __init__(self, directory_path, file_node: FileNode, db, label: Label, image_extensions, logger: logging.Logger):
        self.directory_path = directory_path
        self.file_node = file_node
        self.db = db
        self.label = label
        self.indexed_paths = set()
        self.image_extensions = image_extensions
        self.logger = logger

    def index(self):
        self.logger.info(f"Indexing directory: {self.directory_path}")
        self._index_files_and_directories()
        self._remove_missing_entries()
        self.logger.info("Indexing and cleanup completed.")

    def _add_or_get_label(self, label_name):
        try:
            label = self.label.query.filter_by(name=label_name).first()
            if label is None:
                label = self.label(name=label_name)
                self.db.session.add(label)
                self.db.session.commit()
            return label
        except Exception as e:
            self.logger.error(f"Error adding or retrieving label {label_name}: {e}")

    def _index_files_and_directories(self):
        try:
            for root, dirs, files in os.walk(self.directory_path):
                for name in dirs:
                    self._handle_directory(name, root)
                for name in files:
                    self._handle_file(name, root)
            self.db.session.commit()
        except FileNotFoundError as e:
            self.logger.error(f"File not found: {e}")
        except PermissionError as e:
            self.logger.error(f"Permission denied: {e}")
        except Exception as e:
            self.logger.error(f"Error during file/directory indexing: {e}")

    def _handle_directory(self, name, root):
        path = os.path.join(root, name)
        rel_path = os.path.relpath(path, start=self.directory_path)
        self.indexed_paths.add(rel_path)
        self._create_or_update_node(name, rel_path, is_directory=True)

    def _handle_file(self, name, root):
        is_image = any(name.lower().endswith(ext) for ext in self.image_extensions)
        if not is_image:
            self.logger.info(f"Skipping non-image file {name}")
            return
        path = os.path.join(root, name)
        rel_path = os.path.relpath(path, start=self.directory_path)
        self.indexed_paths.add(rel_path)
        self._create_or_update_node(name, rel_path, is_directory=False, is_image=is_image)

    def _create_or_update_node(self, name, rel_path, is_directory, is_image=False):
        parent_path = os.path.dirname(rel_path)
        parent = None
        if parent_path and parent_path != '.':
            parent = self.file_node.query.filter_by(path=parent_path).first()

        existing_node = self.file_node.query.filter_by(path=rel_path).first()
        if existing_node:
            self.logger.debug(f"Path already indexed: {rel_path}")
            return

        new_node = self.file_node(name=name, path=rel_path, parent=parent, is_image=is_image)
        if not is_directory:
            path_parts = rel_path.split(os.sep)[:-1]  # Exclude the file name itself
            for part in path_parts:
                label = self._add_or_get_label(part)
                new_node.labels.append(label)
        self.db.session.add(new_node)

    def _remove_missing_entries(self):
        all_nodes = self.file_node.query.all()
        for node in all_nodes:
            if node.path not in self.indexed_paths:
                self.logger.info(f"Removing missing file from index: {node.path}")
                self.db.session.delete(node)
        self.db.session.commit()
