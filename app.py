import logging

from flask import Flask, send_file, make_response, render_template, jsonify, request
from flask_cors import CORS
from datetime import datetime, timedelta
from models import db, FileNode, Label
import config
import os

class AppContext:
    def __init__(self):
        self.app = Flask(__name__)
        self.configure_app()
        self.init_db()
        self.indexer = None  # Initialize indexer to None
        self.last_index_time = None  # Initialize last index time
        gunicorn_logger = logging.getLogger('gunicorn.error')
        self.app.logger.handlers = gunicorn_logger.handlers
        self.app.logger.setLevel(gunicorn_logger.level)
        self.logger = self.app.logger

    def configure_app(self):
        self.app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
        self.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.SQLALCHEMY_TRACK_MODIFICATIONS
        CORS(self.app)

    def init_db(self):
        from models import db, FileNode, Label
        db.init_app(self.app)
        with self.app.app_context():
            db.create_all()
        self.db = db
        self.FileNode = FileNode
        self.Label = Label

    def perform_indexing(self, force_run=False):
        # Determine if indexing should be performed based on the time since the last index
        now = datetime.now()
        should_run = force_run or self.last_index_time is None or (now - self.last_index_time) > timedelta(hours=24)

        if should_run:
            from directory_indexer import DirectoryIndexer
            with self.app.app_context():
                self.indexer = DirectoryIndexer(directory_path=config.IMAGE_DIRECTORY, file_node=self.FileNode,
                                                db=self.db, label=self.Label, image_extensions=config.IMAGE_EXTENSIONS,
                                                logger = app.logger)
                self.indexer.index()
            self.last_index_time = now  # Update the timestamp after successful indexing
            self.logger.info(f"Indexing performed at {self.last_index_time}")
            return True, self.last_index_time  # Indexing was performed
        else:
            self.logger.info(f"Indexing skipped. Last indexed at {self.last_index_time}")
            return False, self.last_index_time  # Indexing was skipped

# app, db, FileNode, Label = create_app()
context = AppContext()
app = context.app
db = context.db
context.perform_indexing(True)

@app.before_request
def before_request_logging():
    app.logger.debug(f"Incoming request: {request.method} {request.path}")

@app.after_request
def after_request_logging(response):
    app.logger.debug(f"Outgoing response: {response.status}")
    return response

@app.route('/', methods=['GET'])
def hello():
    app.logger.info("Route / hit with GET request")
    indexed, timestamp = context.perform_indexing()
    action = "Performed indexing" if indexed else "Skipped indexing"
    app.logger.info(f"{action}, last run at {timestamp}")
    return render_template('index.html')

@app.route('/files', methods=['GET'])
def list_files():
    app.logger.info("Route /files hit with GET request")
    tag_query = request.args.get('tag')
    if tag_query:
        app.logger.info(f"Listing files filtered by tag: {tag_query}")
        # Assuming a many-to-many relationship between FileNode and Label
        leaf_files = FileNode.query.join(FileNode.labels).filter(Label.name.contains(tag_query), ~FileNode.children.any()).all()
    else:
        app.logger.info("Listing all leaf files")
        leaf_files = FileNode.query.filter(~FileNode.children.any()).all()

    files_data = [{
        'id': file.id,
        'name': file.name,
        'path': file.path,
        'is_image': file.is_image,
        'parent_id': file.parent_id,
        'labels': [{'id': label.id, 'name': label.name} for label in file.labels]
    } for file in leaf_files]

    return jsonify(files_data)

@app.route('/download/<int:file_id>', methods=['GET'])
def download_file(file_id):
    app.logger.info(f"Route /download/{file_id} hit with GET request")
    file_node = FileNode.query.get_or_404(file_id)
    file_path = os.path.join(config.IMAGE_DIRECTORY, file_node.path)

    if not os.path.exists(file_path):
        app.logger.warning(f'File with ID {file_id} not found at path: {file_path}')
        return jsonify({'error': 'File not found'}), 404

    try:
        response = make_response(send_file(file_path, as_attachment=False, download_name=file_node.name))
        expires = datetime.utcnow() + timedelta(days=1)
        response.headers['Cache-Control'] = 'public, max-age=86400'
        response.headers['Expires'] = expires.strftime("%a, %d %b %Y %H:%M:%S GMT")
        app.logger.info(f"File with ID {file_id} served for download.")
        return response
    except Exception as e:
        app.logger.error(f'Error serving file with ID {file_id}: {e}', exc_info=True)
        return jsonify({'error': 'Error downloading file', 'message': str(e)}), 500

@app.route('/files/label/<int:file_id>', methods=['PUT'])
def add_or_update_label(file_id):
    app.logger.info(f"Route /files/label/{file_id} hit with PUT request")
    data = request.json
    label_name = data.get('label_name', '').strip()

    if not label_name:
        app.logger.error(f"Label name missing for file_id: {file_id}")
        return jsonify({'error': 'Missing label_name'}), 400

    file_node = FileNode.query.get_or_404(file_id)
    label = Label.query.filter_by(name=label_name).first()

    if not label:
        label = Label(name=label_name)
        db.session.add(label)

    if label not in file_node.labels:
        file_node.labels.append(label)
        db.session.commit()
        action = 'Added'
    else:
        action = 'Exists'

    app.logger.info(f"Label '{label_name}' {action} to file_id: {file_id}")
    return jsonify({'message': f'Label {action.lower()} successfully', 'file_id': file_id, 'label_name': label_name}), 200

@app.route('/files/label/<int:file_id>', methods=['POST'])
def add_multiple_labels(file_id):
    app.logger.info(f"Route /files/label/{file_id} hit with POST request")
    data = request.json
    label_names = data.get('label_names')

    if not label_names or not isinstance(label_names, list) or not all(isinstance(name, str) for name in label_names):
        app.logger.error(f"Invalid label_names list provided for file_id: {file_id}")
        return jsonify({'error': 'Missing or invalid label_names list'}), 400

    file_node = FileNode.query.get_or_404(file_id)
    added_labels = []

    for label_name in label_names:
        label_name = label_name.strip()
        if label_name:
            label = Label.query.filter_by(name=label_name).first()
            if not label:
                label = Label(name=label_name)
                db.session.add(label)
            if label not in file_node.labels:
                file_node.labels.append(label)
                added_labels.append(label_name)

    db.session.commit()
    app.logger.info(f"Labels {', '.join(added_labels)} added to file_id: {file_id}")
    return jsonify({'message': 'Labels added successfully', 'file_id': file_id, 'label_names': added_labels}), 200

@app.route('/files/label/<int:file_id>', methods=['GET'])
def get_labels(file_id):
    app.logger.info(f"Route /files/label/{file_id} hit with GET request")
    file_node = FileNode.query.get_or_404(file_id)
    labels = [{'id': label.id, 'name': label.name} for label in file_node.labels]
    app.logger.info(f"Retrieved {len(labels)} labels for file_id: {file_id}")
    return jsonify({'file_id': file_id, 'labels': labels}), 200

@app.route('/files/label/<int:file_id>/<int:label_id>', methods=['DELETE'])
def delete_label(file_id, label_id):
    app.logger.info(f"Route /files/label/{file_id}/{label_id} hit with DELETE request")
    file_node = FileNode.query.get_or_404(file_id)
    label = Label.query.get_or_404(label_id)

    if label in file_node.labels:
        file_node.labels.remove(label)
        db.session.commit()
        return jsonify({'message': 'Label removed successfully', 'file_id': file_id, 'label_id': label_id}), 200
    else:
        return jsonify({'error': 'Label not associated with this file'}), 400

@app.route('/reindex', methods=['POST'])
def reindex():
    app.logger.info(f"Route /reindex hit with POST request")
    indexed, timestamp = context.perform_indexing(force_run=True)
    action = "Reindexing performed" if indexed else "Reindexing skipped"
    app.logger.info(f"{action}, last run at {timestamp}")
    message = f"{action}, last run at {timestamp}" if indexed else "No reindexing needed at this time."
    return jsonify({"message": message}), 200

def add_or_get_label(label_name, db_session):
    """Add a new label to the database or retrieve it if it already exists."""
    label = Label.query.filter_by(name=label_name).first()
    if label is None:
        label = Label(name=label_name)
        db_session.add(label)
        db_session.commit()
        app.logger.info(f"Added new label: {label_name}")
    else:
        app.logger.info(f"Retrieved existing label: {label_name}")
    return label

def find_nodes_by_label(label_name, db_session):
    label = Label.query.filter_by(name=label_name).first()
    if label:
        nodes = label.file_nodes.all()
        app.logger.info(f"Found {len(nodes)} nodes for label '{label_name}'")
        return nodes
    else:
        app.logger.info(f"No nodes found for label '{label_name}'")
        return []

if __name__ == '__main__':
    context.perform_indexing(force_run=True)
    context.app.run(debug=True)
