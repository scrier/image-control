#!/bin/bash

# Default values
DEFAULT_SOURCE_IMAGES="/Users/scrier/Dropbox/Huset/Kartonger"
DEFAULT_DATA_DIRECTORY=$(pwd)

# Prompt for SOURCE_IMAGES with default
read -p "Enter source images directory [$DEFAULT_SOURCE_IMAGES]: " SOURCE_IMAGES
SOURCE_IMAGES=${SOURCE_IMAGES:-$DEFAULT_SOURCE_IMAGES}

# Prompt for DATA_DIRECTORY with default
read -p "Enter data directory [$DEFAULT_DATA_DIRECTORY]: " DATA_DIRECTORY
DATA_DIRECTORY=${DATA_DIRECTORY:-$DEFAULT_DATA_DIRECTORY}

# Run docker command with input values
docker run --rm -it -p 5000:5000 -v "$DATA_DIRECTORY":/data -v "$SOURCE_IMAGES":/images $(docker build -q .)
