# README for Image Helper

## Overview

Image Helper is a Flask-based web application designed to manage and view image files. It facilitates indexing images 
stored in a specified directory, tagging images with labels for easy categorization, and directly viewing images in the 
browser. The application supports searching for images based on tags and managing image metadata.

## Features
 * **Automatic Image Indexing**: Scans a specified directory for image files and indexes them for accessibility via a web interface.
 * **Label Management**: Enables adding, updating, and removing labels associated with each image, allowing for effective categorization.
 * **Search Functionality**: Users can search for images based on associated labels.
 * **Direct Image Viewing**: Allows for direct viewing of images in the browser, enhancing user experience.
 * **Responsive Design**: Features a user-friendly and responsive web interface for various devices and screen sizes.

## Installation

### Prerequisites

 * Python 3.10 or higher
 * Docker

### Setup Instructions

1. Clone the Repository:
```bash
git clone https://your-repository-url/image-helper.git
cd image-helper
```

2. Configuration:
    * **IMAGE_DIRECTORY**: Path where your images are stored.
    * **SQLALCHEMY_DATABASE_URI**: Database connection URI.
    * **IMAGE_EXTENSIONS**: Supported image file extensions. 
   
   Update the config.yml with the appropriate settings or use environment variables.

3. Build and Run with Docker:

Use the provided Dockerfile to build and run the application. The `run.sh` script facilitates the Docker process.

Execute the script:

```bash
./run.sh
```

Follow the prompts to specify the source images directory and the data directory for the database.

## Running the Application

To run Image Helper with Docker, ensuring both the image directory and the SQLite database file are correctly mounted into the container:

1. **Prepare Volume Mounts**:
   * Ensure the **IMAGE_DIRECTORY** and the directory for the SQLite database file (_/data_) are accessible.
2. **Execute _run.sh_ Script**:
 
   This script prompts for the source images directory and the data directory, then runs the Docker container with these directories mounted appropriately.

```bash
./run.sh
```

## Database and Models

The application uses SQLAlchemy for ORM with three main models:

 * **Label**: Represents tags or labels that can be associated with image files.
 * **FileNode**: Represents an image or directory in the file system.
 * **FileNodeLabelAssociation**: A helper association table for the many-to-many relationship between FileNode and Label.

## Accessing the Application

After starting the application via Docker, access the web interface by navigating to http://localhost:5000 in your web browser.

## Development

For development, set the Flask environment to development for live reloading and detailed debug information:

```bash
export FLASK_ENV=development
flask run
```

## Support

For support, feature requests, or contributions, please open an issue in the GitLab repository for this project.
