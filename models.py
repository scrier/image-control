from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()  # If db is initialized in your main app, import it instead

class Label(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)

class FileNodeLabelAssociation(db.Model):
    __tablename__ = 'file_node_label_association'
    file_node_id = db.Column(db.Integer, db.ForeignKey('file_node.id'), primary_key=True)
    label_id = db.Column(db.Integer, db.ForeignKey('label.id'), primary_key=True)

class FileNode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    path = db.Column(db.String(512), nullable=False, unique=True)
    is_image = db.Column(db.Boolean, default=False, nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('file_node.id'), nullable=True)
    children = db.relationship('FileNode', backref=db.backref('parent', remote_side=[id]), lazy='dynamic')
    labels = db.relationship('Label', secondary='file_node_label_association', backref=db.backref('file_nodes', lazy='dynamic'))

    def __repr__(self):
        return f'<FileNode {self.name}>'
