import os
import yaml

basedir = os.path.abspath(os.path.dirname(__file__))


def load_config():
    default_config_path = os.path.join(basedir, os.getenv('CONFIG_FILE', 'test_config.yml'))
    print(f"Attempting to load configuration from {default_config_path}")
    config = {}  # Start with defaults
    if os.path.exists(default_config_path):
        with open(default_config_path, 'r') as file:
            config = yaml.safe_load(file)
            print(f"Configuration loaded from file: {config}")

    # Override with environment variables if they exist
    config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI',
                                                  config.get('SQLALCHEMY_DATABASE_URI', 'sqlite:///app.db'))
    config['SQLALCHEMY_TRACK_MODIFICATIONS'] = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS',
                                                         str(config.get('SQLALCHEMY_TRACK_MODIFICATIONS',
                                                                        False))).lower() == 'true'
    config['IMAGE_EXTENSIONS'] = config.get('IMAGE_EXTENSIONS', ['.png', '.jpg', '.jpeg', '.gif', '.bmp'])

    print(f"Final configuration: {config}")
    return config

config = load_config()

SQLALCHEMY_DATABASE_URI = config['SQLALCHEMY_DATABASE_URI']
SQLALCHEMY_TRACK_MODIFICATIONS = config['SQLALCHEMY_TRACK_MODIFICATIONS']
IMAGE_EXTENSIONS = config['IMAGE_EXTENSIONS']
IMAGE_DIRECTORY = config.get('IMAGE_DIRECTORY', '/default/path/to/your/files')
